import cv2	# OpenCV
from skimage.metrics import structural_similarity # Для получения разностного изображения
import imutils # Для поиска и отрисовки контуров
import numpy as np	# Для конечного результата

trshld_lvl = 30 # Пороговое значение
c_clr = (0, 0, 255) # Цвет выделения
c_thick = 1 # Толщина выделения

# Загрузка двух изображений
imageA = cv2.imread('a.bmp')
imageB = cv2.imread('b.bmp')

cv2.imshow("The first one", imageA)
cv2.imshow("The second one", imageB)

# Переход к шкале серого изображения
grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

# Получение разностного изображения [0...255] через вычисление индекса структурного сходства (SSIM)
diff = structural_similarity(grayA, grayB, full=True)[1]
diff = (diff * 255).astype("uint8")

# Отсечение по пороговому уровню
thresh = cv2.threshold(diff, trshld_lvl, 255, cv2.THRESH_BINARY)[1]
cv2.imshow("Difference", thresh )
thresh = cv2.threshold(thresh, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

# Поиск контура для отрисовки
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)

# Цикл для поиска и отрисовки прямоугольного контура вокруг различных областей у изображений
for c in cnts:
	(x, y, w, h) = cv2.boundingRect(c)
	cv2.rectangle(imageA, (x, y), (x + w, y + h ), c_clr , c_thick )
	cv2.rectangle(imageB, (x, y), (x + w, y + h ), c_clr , c_thick )

# Вывод и сохранение результата
res = np.concatenate((imageA, imageB), axis=1)
cv2.imshow("Result", res) 
cv2.imwrite('Result.bmp', res)

cv2.waitKey(0)